<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>TS2 PSS Diagnostics</name>
  <macros>
    <GPLC>KG-GTA:PSS-GPLC-1</GPLC>
    <PLC>KG-GTA:PSS-PLC-1</PLC>
    <WIRE_PY>scripts/wire.py</WIRE_PY>
  </macros>
  <width>1450</width>
  <height>886</height>
  <widget type="rectangle" version="2.0.0">
    <name>Title Rectangle</name>
    <class>TITLE-BAR</class>
    <x use_class="true">0</x>
    <y use_class="true">0</y>
    <width>1450</width>
    <height use_class="true">50</height>
    <line_width use_class="true">0</line_width>
    <background_color use_class="true">
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Title</name>
    <class>TITLE</class>
    <text>TS2 PSS Diagnostics</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>550</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>EPICS</name>
    <file>embedded_displays/diagnostics/epics.bob</file>
    <x>20</x>
    <y>70</y>
    <width>370</width>
    <height>244</height>
    <resize>2</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>PLC Software</name>
    <file>embedded_displays/diagnostics/plc_software.bob</file>
    <x>20</x>
    <y>324</y>
    <width>395</width>
    <height>182</height>
    <resize>2</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>TS2 PSS Electrical Cabinet</name>
    <macros>
      <CABINET>Cabinet</CABINET>
      <P>TS2-010Row:CnPw-U-012</P>
    </macros>
    <file>embedded_displays/diagnostics/Cabinet.bob</file>
    <x>75</x>
    <y>516</y>
    <width>260</width>
    <height>360</height>
    <resize>2</resize>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>GCPU-CPU Wire</name>
    <x>530</x>
    <y>325</y>
    <width>250</width>
    <height>40</height>
    <points>
      <point x="0.0" y="0.0">
      </point>
      <point x="0.0" y="40.0">
      </point>
      <point x="250.0" y="40.0">
      </point>
      <point x="250.0" y="0.0">
      </point>
    </points>
    <line_color>
      <color name="DISCONNECTED" red="105" green="77" blue="164">
      </color>
    </line_color>
    <line_style>1</line_style>
    <scripts>
      <script file="$(WIRE_PY)" check_connections="false">
        <pv_name>$(GPLC):Proc_GW_Comm</pv_name>
      </script>
    </scripts>
    <tooltip>N/A</tooltip>
  </widget>
  <widget type="group" version="2.0.0">
    <name>GCPU</name>
    <x>450</x>
    <y>60</y>
    <width>162</width>
    <height>270</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>Title</name>
      <text>Gateway CPU</text>
      <width>162</width>
      <height>40</height>
      <font>
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <wrap_words>false</wrap_words>
    </widget>
    <widget type="picture" version="2.0.0">
      <name>GCPU</name>
      <file>symbols/plc/GCPU.png</file>
      <y>40</y>
      <width>162</width>
      <height>230</height>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>Diag</name>
      <macros>
        <DIAG_CONN_STAT>$(GPLC):CPU_ConnStat</DIAG_CONN_STAT>
        <DIAG_STAT>$(GPLC):CPU_Stat</DIAG_STAT>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>67</x>
      <y>83</y>
      <width>30</width>
      <height>70</height>
      <visible>false</visible>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>F-CPU Wire</name>
    <x>803</x>
    <y>325</y>
    <width>225</width>
    <height>40</height>
    <points>
      <point x="0.0" y="0.0">
      </point>
      <point x="0.0" y="40.0">
      </point>
      <point x="225.0" y="40.0">
      </point>
      <point x="225.0" y="0.0">
      </point>
    </points>
    <line_color>
      <color name="DISCONNECTED" red="105" green="77" blue="164">
      </color>
    </line_color>
    <line_style>1</line_style>
    <scripts>
      <script file="$(WIRE_PY)" check_connections="false">
        <pv_name>$(PLC):CPU_ConnStat</pv_name>
      </script>
    </scripts>
    <tooltip>N/A</tooltip>
  </widget>
  <widget type="group" version="2.0.0">
    <name>CPU</name>
    <x>722</x>
    <y>60</y>
    <width>162</width>
    <height>270</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>Title</name>
      <text>F-CPU</text>
      <width>162</width>
      <height>40</height>
      <font>
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <wrap_words>false</wrap_words>
    </widget>
    <widget type="picture" version="2.0.0">
      <name>CPU</name>
      <file>symbols/plc/CPU.png</file>
      <y>40</y>
      <width>162</width>
      <height>230</height>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>Diag</name>
      <macros>
        <DIAG_CONN_STAT>$(GPLC):Proc_GW_Comm</DIAG_CONN_STAT>
        <DIAG_STAT>$(PLC):CPU_Stat</DIAG_STAT>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>67</x>
      <y>83</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>HMI Wire</name>
    <x>1074</x>
    <y>325</y>
    <width>239</width>
    <height>40</height>
    <points>
      <point x="0.0" y="0.0">
      </point>
      <point x="0.0" y="40.0">
      </point>
      <point x="239.0" y="40.0">
      </point>
      <point x="239.0" y="0.0">
      </point>
    </points>
    <line_color>
      <color name="DISCONNECTED" red="105" green="77" blue="164">
      </color>
    </line_color>
    <line_style>1</line_style>
    <scripts>
      <script file="$(WIRE_PY)" check_connections="false">
        <pv_name>$(PLC):HMI_ConnStat</pv_name>
      </script>
    </scripts>
    <tooltip>N/A</tooltip>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>Remote I/O Wire</name>
    <x>590</x>
    <y>325</y>
    <width>464</width>
    <height>215</height>
    <points>
      <point x="0.0" y="215.0">
      </point>
      <point x="0.0" y="75.0">
      </point>
      <point x="464.0" y="75.0">
      </point>
      <point x="464.0" y="0.0">
      </point>
    </points>
    <line_color>
      <color name="DISCONNECTED" red="105" green="77" blue="164">
      </color>
    </line_color>
    <line_style>1</line_style>
    <scripts>
      <script file="$(WIRE_PY)" check_connections="false">
        <pv_name>$(PLC):RIO_IM_ConnStat</pv_name>
      </script>
    </scripts>
    <tooltip>N/A</tooltip>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Switch</name>
    <x>994</x>
    <y>60</y>
    <width>120</width>
    <height>270</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>Title</name>
      <text>Scalance</text>
      <width>120</width>
      <height>40</height>
      <font>
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <wrap_words>false</wrap_words>
    </widget>
    <widget type="picture" version="2.0.0">
      <name>Switch</name>
      <file>symbols/plc/Switch.png</file>
      <y>40</y>
      <width>120</width>
      <height>230</height>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>Diag</name>
      <macros>
        <DIAG_CONN_STAT>$(PLC):CPU_ConnStat</DIAG_CONN_STAT>
        <DIAG_STAT>$(PLC):CPU_Stat</DIAG_STAT>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>45</x>
      <y>83</y>
      <width>30</width>
      <height>70</height>
      <visible>false</visible>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="symbol" version="2.0.0">
      <name>Disconnected</name>
      <pv_name>$(PLC):CPU_ConnStat</pv_name>
      <symbols>
        <symbol>$(ESS_SYMBOLS=/ess-symbols)/badges/alarms/unconnected@32.png</symbol>
      </symbols>
      <x>48</x>
      <y>86</y>
      <width>24</width>
      <height>24</height>
      <scripts>
        <script file="scripts/diag_conn_stat.py" check_connections="false">
          <pv_name>$(pv_name)</pv_name>
        </script>
      </scripts>
      <tooltip>N/A</tooltip>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>HMI</name>
    <x>1208</x>
    <y>60</y>
    <width>210</width>
    <height>270</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="picture" version="2.0.0">
      <name>HMI</name>
      <file>symbols/plc/HMI.png</file>
      <y>40</y>
      <width>210</width>
      <height>230</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Title</name>
      <text>HMI</text>
      <width>210</width>
      <height>40</height>
      <font>
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <wrap_words>false</wrap_words>
    </widget>
    <widget type="symbol" version="2.0.0">
      <name>Disconnected</name>
      <pv_name>$(PLC):HMI_ConnStat</pv_name>
      <symbols>
        <symbol>$(ESS_SYMBOLS=/ess-symbols)/badges/alarms/unconnected@32.png</symbol>
      </symbols>
      <x>89</x>
      <y>93</y>
      <width>24</width>
      <height>24</height>
      <scripts>
        <script file="scripts/diag_conn_stat.py" check_connections="false">
          <pv_name>$(pv_name)</pv_name>
        </script>
      </scripts>
      <tooltip>N/A</tooltip>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Remote I/O</name>
    <macros>
      <DIAG_CONN_STAT>$(PLC):$(MOD)_ConnStat</DIAG_CONN_STAT>
      <DIAG_STAT>$(PLC):$(MOD)_Stat</DIAG_STAT>
    </macros>
    <x>530</x>
    <y>450</y>
    <width>900</width>
    <height>390</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>Title</name>
      <text>Remote I/O</text>
      <width>900</width>
      <height>40</height>
      <font>
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <wrap_words>false</wrap_words>
    </widget>
    <widget type="picture" version="2.0.0">
      <name>RIO</name>
      <file>symbols/plc/RIO.png</file>
      <y>40</y>
      <width>900</width>
      <height>350</height>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>IM_Diag</name>
      <macros>
        <MOD>RIO_IM</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>82</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(MOD)_Diag</name>
      <macros>
        <MOD>FDI1</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>130</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(MOD)_Diag</name>
      <macros>
        <MOD>FDI2</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>173</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(MOD)_Diag</name>
      <macros>
        <MOD>FDI3</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>215</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(MOD)_Diag</name>
      <macros>
        <MOD>FDI4</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>258</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(MOD)_Diag</name>
      <macros>
        <MOD>FDI5</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>302</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(MOD)_Diag</name>
      <macros>
        <MOD>FDI6</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>345</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(MOD)_Diag</name>
      <macros>
        <MOD>FDI7</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>388</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(MOD)_Diag</name>
      <macros>
        <MOD>FDI8</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>431</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(MOD)_Diag</name>
      <macros>
        <MOD>DI1</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>474</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(MOD)_Diag</name>
      <macros>
        <MOD>DI2</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>518</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(MOD)_Diag</name>
      <macros>
        <MOD>DI3</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>560</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(MOD)_Diag</name>
      <macros>
        <MOD>AI1</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>605</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(MOD)_Diag</name>
      <macros>
        <MOD>FDQ1</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>648</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(MOD)_Diag</name>
      <macros>
        <MOD>FDQ2</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>690</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(MOD)_Diag</name>
      <macros>
        <MOD>FDQ3</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>734</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(MOD)_Diag</name>
      <macros>
        <MOD>DQ1</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>777</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(MOD)_Diag</name>
      <macros>
        <MOD>DQ2</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>820</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>$(MOD)_Diag</name>
      <macros>
        <MOD>DQ3</MOD>
      </macros>
      <file>embedded_displays/diagnostics/PLC_module_status.bob</file>
      <x>863</x>
      <y>275</y>
      <width>30</width>
      <height>70</height>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
  </widget>
  <widget type="label" version="2.0.0">
    <name>TimeDeviation</name>
    <text>Time difference:</text>
    <x>600</x>
    <y>310</y>
    <width>120</width>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>FcpuTimeDeviation</name>
    <pv_name>$(PLC):FcpuTimeDeviation</pv_name>
    <x>600</x>
    <y>333</y>
    <width>105</width>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
</display>
