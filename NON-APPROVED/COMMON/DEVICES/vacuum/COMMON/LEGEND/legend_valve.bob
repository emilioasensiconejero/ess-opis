<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>$(vacDEV) - Vacuum Valve Legend</name>
  <macros>
    <ROOT>$(ESS_OPIS=/ess-opis)/NON-APPROVED</ROOT>
    <WIDGET_ROOT>$(ROOT)/COMMON/DEVICES/vacuum</WIDGET_ROOT>
    <vacSYMBOLS>$(ROOT)/COMMON/DEVICES/vacuum/symbols</vacSYMBOLS>
  </macros>
  <width>410</width>
  <height>250</height>
  <widget type="label" version="2.0.0">
    <name>Title</name>
    <text>$(TITLE)</text>
    <width>410</width>
    <height>60</height>
    <font>
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color>
      <color name="White" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <background_color>
      <color name="Primary Blue" red="0" green="148" blue="202">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Tip</name>
    <text>Check the tooltips for explanations!</text>
    <x>10</x>
    <y>70</y>
    <width>390</width>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Group - Block icon</name>
    <x>67</x>
    <y>100</y>
    <width>266</width>
    <height>140</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>Device</name>
      <text>$(vacDEV)</text>
      <width>80</width>
      <height>40</height>
      <font>
        <font name="LARGE-MONO-PLAIN" family="Source Code Pro" style="REGULAR" size="21.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>Type of device
sec-subsec:dis-DEV-idx</tooltip>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Index</name>
      <text>IDX</text>
      <y>60</y>
      <width>80</width>
      <height>40</height>
      <font>
        <font name="LARGE-MONO-PLAIN" family="Source Code Pro" style="REGULAR" size="21.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>The index part of device name
sec-subsec:dis-dev-IDX</tooltip>
    </widget>
    <widget type="symbol" version="2.0.0">
      <name>Symbol</name>
      <pv_name>sim://ramp(0,4,2)</pv_name>
      <symbols>
        <symbol>$(vacSYMBOLS)/$(vacDEV)$(FLAVOR=)/$(vacDEV)-invalid.png</symbol>
        <symbol>$(vacSYMBOLS)/$(vacDEV)$(FLAVOR=)/$(vacDEV)-undefined.png</symbol>
        <symbol>$(vacSYMBOLS)/$(vacDEV)$(FLAVOR=)/$(vacDEV)-open.png</symbol>
        <symbol>$(vacSYMBOLS)/$(vacDEV)$(FLAVOR=)/$(vacDEV)-closed.png</symbol>
        <symbol>$(vacSYMBOLS)/$(vacDEV)$(FLAVOR=)/$(vacDEV)-error.png</symbol>
      </symbols>
      <x>96</x>
      <y>3</y>
      <width>92</width>
      <height>92</height>
      <actions>
      </actions>
      <tooltip></tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Open Symbol Legend</name>
      <actions>
        <action type="open_display">
          <file>legend_valve_symbol.bob</file>
          <target>standalone</target>
          <description>Open Symbol Legend</description>
        </action>
      </actions>
      <text></text>
      <x>96</x>
      <y>3</y>
      <width>92</width>
      <height>92</height>
      <transparent>true</transparent>
      <tooltip>Symbol
GREEN - OPEN
BLUE    - UNDEFINED
WHITE  - CLOSED
RED      - ERROR
GRAY    - INVALID</tooltip>
    </widget>
    <widget type="symbol" version="2.0.0">
      <name>Interlock_badge</name>
      <pv_name>sim://ramp(0,4,2)</pv_name>
      <symbols>
        <symbol>$(vacSYMBOLS)/interlock/interlock-invalid.png</symbol>
        <symbol>$(vacSYMBOLS)/interlock/interlock-healthy.png</symbol>
        <symbol>$(vacSYMBOLS)/interlock/interlock-tripped.png</symbol>
        <symbol>$(vacSYMBOLS)/interlock/interlock-overridden.png</symbol>
        <symbol>$(vacSYMBOLS)/interlock/interlock-disabled.png</symbol>
      </symbols>
      <x>92</x>
      <y>104</y>
      <width>32</width>
      <height>32</height>
      <tooltip></tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Open Interlock Legend</name>
      <actions>
        <action type="open_display">
          <file>legend_interlocks.bob</file>
          <target>standalone</target>
          <description>Open Interlock Legend</description>
        </action>
      </actions>
      <text></text>
      <x>92</x>
      <y>104</y>
      <width>32</width>
      <height>32</height>
      <transparent>true</transparent>
      <tooltip>Interlock status
Clickable - will show a window with the status of all the various interlocks
GREEN   - HEALTHY
YELLOW - OVERRIDDEN
WHITE    - DISABLED
RED        - TRIPPED
GRAY      - INVALID</tooltip>
    </widget>
    <widget type="picture" version="2.0.0">
      <name>Badge Locked</name>
      <file>$(ESS_SYMBOLS=/ess-symbols)/badges/vacuum/lock@32.png</file>
      <x>198</x>
      <width>32</width>
      <height>32</height>
      <tooltip>Automatic Open Disabled</tooltip>
    </widget>
    <widget type="picture" version="2.0.0">
      <name>Badge Started</name>
      <file>$(ESS_SYMBOLS=/ess-symbols)/badges/vacuum/started@32.png</file>
      <x>198</x>
      <y>68</y>
      <width>32</width>
      <height>32</height>
      <tooltip>Opened</tooltip>
    </widget>
    <widget type="picture" version="2.0.0">
      <name>Badge Manual</name>
      <file>$(ESS_SYMBOLS=/ess-symbols)/badges/vacuum/manual@32.png</file>
      <x>232</x>
      <y>68</y>
      <width>32</width>
      <height>32</height>
      <tooltip>Manual Control Enabled</tooltip>
    </widget>
    <widget type="picture" version="2.0.0">
      <name>Badge Warning</name>
      <file>$(ESS_SYMBOLS=/ess-symbols)/badges/vacuum/warning@32.png</file>
      <x>198</x>
      <y>104</y>
      <width>32</width>
      <height>32</height>
      <tooltip>Warning
Clickable - Will show a window with the warning and error status and message</tooltip>
    </widget>
  </widget>
</display>
